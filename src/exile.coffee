info =
  majorVersion: 0
  minorVersion: 3
  codename: 'curse-work-async'

modules =
  utility: require './utility'
  xl: require './xl'
  content: require './content'
  http: require 'http'
  url: require 'url'
  querystring: require 'querystring'

Server = (serverMap) ->
  #Server mapping data
  mappingData = { }
    
  #Status messages for throwing errors and such
  serverStatusMessages = 
    buildErrorMessage: (code, context) ->
      if not modules.http.STATUS_CODES[code]?
        code = 500
      context.model = 
        code: code
        desc: modules.http.STATUS_CODES[code]
        date: new Date().toLocaleString()
        minorVersion: info.minorVersion
        majorVersion: info.majorVersion
        codename: info.codename

      try
        modules.content.readSystemResource('generateError.xl', context)
      catch error
       console.log error
       '<div>' + code + ': ' + modules.http.STATUS_CODES[code] + '</div><div>Exile web server</div><div>(Server was unable to find "./system/generateError.xl" - check server\'s files!)</div>'

  mapServerUrls = (serverMap) ->
    mappingData =
      mappedUrls: 0
      urlFileHash: {}
    for key of serverMap
      mappingData.urlFileHash[key] = serverMap[key]
      mappingData.mappedUrls++
      
    mappingData
    
  buildServerContext = (request) ->
    parsed = modules.url.parse request.url
    url: { path: parsed.pathname, args: modules.querystring.parse(parsed.query) }
    doRequest: retrieveResult
    authToken: null
    model: {}
    viewModel: {}
    
  retrieveResult = (url, context, callback) ->    
    retrieveObject = (mappedObject, serverContext) ->
      body: JSON.stringify mappedObject
      type: 'text/json'

    retrieveContent = (mappedObject, serverContext) ->
      body: modules.content.readContent mappedObject, serverContext
      type: modules.content.fileType mappedObject
      
    retrieveService = (mappedObject, serverContext, callback) ->
      respondWith = (result) ->
        switchResponseType(result, serverContext, callback)
      mappedObject(serverContext, respondWith)

    retrieveResource = (url, serverContext) ->
      body: modules.content.readResource(url, serverContext)
      type: modules.content.fileType url
       
    switchResponseType = (responseObject, serverContext, callback) ->
      if typeof responseObject is 'string'
        callback(retrieveContent(responseObject, serverContext), serverContext)
      else if typeof responseObject is 'object'
        callback(retrieveObject(responseObject, serverContext), serverContext)
      else if typeof responseObject is 'function'
        retrieveService responseObject, serverContext, callback
        
    if mappingData.mappedUrls is 0 then throw 418
    targetUrl = modules.url.parse url
    mappedObject = mappingData.urlFileHash[targetUrl.pathname]
    if mappedObject
      switchResponseType mappedObject, context, callback
    else
      callback(retrieveResource url)

  respond = (request, response) ->          
    handleServerError = (request, response, error, context) ->
      body = serverStatusMessages.buildErrorMessage error, context
      response.writeHead error, 'Content-Length' : body.length, 'Content-Type' : 'text/html'
      response.write body

    try
      retrieveResult request.url, buildServerContext(request), (result) -> 
        response.writeHead 200, { 'Content-Length' : result.body.length, 'Content-Type' : result.contentType }
        response.write result.body
        response.end()
    catch error
      if typeof error isnt 'number'
        console.log '=====ERROR LOG====='
        console.log error
        console.log '===ERROR LOG END==='
      handleServerError request, response, error, buildServerContext(request)
      response.end()

  mappingData = mapServerUrls serverMap

  return { 
    listen: (port, host) ->
      try
        server = modules.http.createServer respond
        console.log 'Exile server v' + info.majorVersion+'.' + info.minorVersion + ' listening @ ' + host + ':' + port
        server.listen port, host
      catch err
        console.log 'Unhandled exception!\n' + err
    }

exports.Server = Server
