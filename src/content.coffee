modules =
  fs: require 'fs'
  _: require 'underscore'
  utility: require './utility'
  xl: require './xl'

contentType = 
  'image/png': ['.png']
  'text/html': ['.html', '.xl', '.xlhtml']
  'text/css': ['.css']
  'text/json': ['.json']
  'text/javascript': ['.js']

parsedFileTypes = ['.xlhtml', '.xl']

contentDirectory = './content/'
resourceDirectory = './resources/'
systemDirectory = './system/'

fileType = (path) ->
  for type of contentType
    for ext in contentType[type]
      if modules.utility.endsWith path, ext then return type
  'text/plain'

parsedFileType = (path) ->
  for type in parsedFileTypes
    if modules.utility.endsWith path, type then return true
  false

readContent = (fsPath, serverContext) -> retrieve(contentDirectory + fsPath, serverContext)
readResource = (url, serverContext) -> retrieve(resourceDirectory + url, serverContext)
readSystemResource = (fsPath, serverContext) -> retrieve(systemDirectory + fsPath, serverContext)

retrieve = (contentPath, serverContext) ->
  try
    contentPath = modules.utility.trimEnd contentPath, '/'
    content = modules.fs.readFileSync(contentPath).toString()
  catch error
    console.log error
    throw 404
  if parsedFileType(contentPath) then modules.xl.parse(content, serverContext) else content 
  
exports.readContent = readContent
exports.fileType = fileType
exports.readResource = readResource
exports.readSystemResource = readSystemResource
