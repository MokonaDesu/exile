exports.asStringOf = (obj, depth = 0) ->
  result = ''  
  indent = ''
  for i in [0..depth] then indent += '	'
  for property of obj
    propertyString = if typeof obj[property] is 'object' and depth < 3 then '{' + exports.asStringOf(obj[property], depth + 1) + ' }' else if typeof obj[property] is 'function' then 'method..' else obj[property]
    result += '\n' + indent + property + ': ' + propertyString
  result


exports.endsWith = (str, postfix) ->
  (str.indexOf(postfix) isnt -1) and (str.indexOf postfix is (str.length - postfix.length))

exports.cloneObject = (object) -> 
  JSON.parse JSON.stringify object

`
exports.trimEnd=function(s,c)
{
c = c?c:' ';
var i=s.length-1;
for(;i>=0 && s.charAt(i)==c;i--);
return s.substring(0,i+1);
}
`