_ = require 'underscore'
uuid = require 'node-uuid'

data =
  users: [{xlid: uuid(), first: 'John', last: 'Doe'}, {xlid: uuid(), first: 'Jenna', last: 'Bain'}]
  entries: []
  
findClause = (criteria) ->   
  commit = (location) ->
    searchTargetCollection = (target) ->
      result = []
      for item in target
        if criteria is 'any'
          result.push item
        else 
          for trait of criteria
            if item[trait] is criteria[trait] then result.push item
      result

    if not location? then return []
          
    if (location is 'any')
      result = []
      for collection of data
        result = _.union(result, searchTargetCollection(data[collection]))
      _.uniq result
    else
      if not data[location]? then return []
      searchTargetCollection data[location]
    
  { in: commit, everywhere: -> commit('any') }

insert = (item) ->
  into = (location) ->
    if item?
      if not data[location]? then data[location] = []
      item.xlid = uuid();
      data[location].push item
  { into }
    
exports.find = findClause
exports.insert = insert
