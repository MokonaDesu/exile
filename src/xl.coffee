modules =
  utility: require './utility'
  mustache: require 'mustache'

findModelInMarkup = (content, serverContext) ->
  retrieveTargetObject = (serverContext, action) ->
    JSON.parse(serverContext.doRequest(action).body)

  buildScriptTag = (viewmodel) ->
    '<script> var model = ' +  JSON.stringify(viewmodel) + ' </script>'
    

  modelTag = /<xl\s+model(\s*=\s*"(.+)")?\s*\/\s*>/i.exec content
  if modelTag
    try
      scriptTag = null
      if (modelTag[1])
        # console.log 'building viewmodel from action'
        scriptTag = buildScriptTag(retrieveTargetObject(serverContext, modelTag[2]))
      else
        # console.log 'building viewmodel from context'
        # console.log serverContext.viewModel
        scriptTag = buildScriptTag(serverContext.viewModel)
      return content.replace modelTag[0], scriptTag
    catch error
      console.log error
  content

parse = (content, serverContext) ->
  content = findModelInMarkup content, serverContext
  modules.mustache.render content, serverContext.model

exports.parse = parse
